import io
# import certifi
import urllib.request
import math
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import scikitplot as skplt
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import accuracy_score, classification_report
from sklearn.model_selection import train_test_split
from sklearn.neighbors import KNeighborsClassifier
from sklearn.preprocessing import StandardScaler

# Fase 1 - Prelievo dataset

# dataset_20p_6f_576b_obf_TOY.parquet ds_file = io.BytesIO(urllib.request.urlopen(
# 'http://143.225.229.53:8080/share.cgi?ssid=cb287d7714fc428187835144faecc0b4&fid=cb287d7714fc428187835144faecc0b4
# &filename=dataset_20p_6f_576b_obf_TOY.parquet&openfolder=forcedownload&ep=').read())

# dataset_20p_6f_576b_no_obf_TOY.parquet
#ds_file = io.BytesIO(urllib.request.urlopen(
# 'http://143.225.229.53:8080/share.cgi?ssid=61d98f9c74c04b5d86dbfa006fd23e26&fid=61d98f9c74c04b5d86dbfa006fd23e26'
# '&filename=dataset_20p_6f_576b_no_obf_TOY.parquet&openfolder=forcedownload&ep=').read())

# dataset_20p_6f_576b_obf_FULL.parquet
ds_file = io.BytesIO(urllib.request.urlopen(
  'http://143.225.229.53:8080/share.cgi?ssid=31a305e879644d7b943a5391a0ec0304&fid=31a305e879644d7b943a5391a0ec0304'
  '&filename=dataset_20p_6f_576b_obf_FULL.parquet&openfolder=forcedownload&ep=').read())

# dataset_20p_6f_576b_no_obf_FULL.parquet ds_file = io.BytesIO(urllib.request.urlopen(
# 'http://143.225.229.53:8080/share.cgi?ssid=1b0e364d6997445380b4f14219e6532e&fid=1b0e364d6997445380b4f14219e6532e
# '&filename=dataset_20p_6f_576b_no_obf_FULL.parquet&openfolder=forcedownload&ep=').read())

# local
# df = pd.read_parquet('dataset_20p_6f_576b_obf_TOY.parquet')
# df = pd.read_parquet('dataset_20p_6f_576b_no_obf_TOY.parquet')
# df = pd.read_parquet('dataset_20p_6f_576b_obf_FULL.parquet')
# df = pd.read_parquet('dataset_20p_6f_576b_no_obf_FULL.parquet')



# Fase 2 -  Filtraggio colonne dataframe

# remote
df = pd.read_parquet(ds_file)

y = df['LABEL']  # output

columns_filtered = ['LABEL']  # Features: 'IAT', 'LOAD', 'PL', 'TTL', 'DIR', 'WIN', 'FLG'
df.drop(columns_filtered, axis=1, inplace=True)


# Fase 3 - Selezione dataset features e porzione di apprendimento
columns_features = ['IAT']  # Features: 'IAT', 'LOAD', 'PL', 'TTL', 'DIR', 'WIN', 'FLG'
features_to_shorten = ['LOAD']  # Features i cui elementi sono array troppo
# grandi e quindi è necessario ridurne la dimensione

# X = pd.DataFrame(df['IAT'].tolist())
# X = pd.DataFrame(df['LOAD'].tolist())
# X = pd.concat([X, X2], axis=1)
means = [0, 0, 0, 0, 0]
X = pd.DataFrame()
for feature in columns_features:
    feature_to_add = df[feature].tolist()

    # Check per eventuali feature troppo 'pesanti'
    if feature in features_to_shorten:

        for k in range(0, len(feature_to_add)):
            tmp_ary = feature_to_add[k]  # Prelevo il singolo elemento della colonna
            # (array di cui devo ridurre la dimensione)
            tmp_ary = tmp_ary[tmp_ary != 0]  # Elimino gli elementi nulli

            l = len(tmp_ary)

            indices = [math.floor(l / 5), math.floor(2 * l / 5), math.floor(3 * l / 5),
                       math.floor((4 * l) / 5)]
            ary_grps = np.split(tmp_ary, indices)  # Divido l'array in 5 sottoarray

            for j in range(0, len(ary_grps)):  # Calcolo la media per ognuno dei sottoarray
                # e la inserisco in una lista
                means[j] = np.mean(ary_grps[j])

            feature_to_add[k] = means  # Sostituisco l'array con le medie dei suoi
            # sottoarray
    temp = pd.DataFrame(feature_to_add)
    # print(temp)
    X = pd.concat([X, temp], axis=1)

X_train, X_test, y_train, y_test = train_test_split(X, y,
                                                    test_size=0.15)
# Il dataset di test è composto dal 15% degli esempi complessivi

sc = StandardScaler()  # La riduzione di scala standardizza la serie di valori. La standardizzazione è un metodo di
# riduzione di scala che trasforma una serie di valori in una distribuzione normale standard con media uguale a zero
# e devianza standard uguale a uno
sc.fit(X_train)  # stima la media del campione e la deviazione standard dell'insieme di training (X_train).
X_train_std = sc.transform(X_train)
X_test_std = sc.transform(X_test)


# Fase 4 - Selezione modello e apprendimento algoritmo

#model = KNeighborsClassifier()

model = RandomForestClassifier()

# model = RadiusNeighborsClassifier(radius=200.0)

# model = MLPClassifier(hidden_layer_sizes=[200,200], max_iter=200, tol=0.000001)

# model = Perceptron(max_iter=200, tol=0.0000000001, eta0=0.00001, random_state=0)

# X_train = pd.DataFrame(X_train['LOAD'].tolist())
# X_test = pd.DataFrame(X_test['LOAD'].tolist())

model.fit(X_train_std, y_train)  # comincio l'addestramento del modello


# Fase 5 - Verifica modello e analisi prestazoni

p_test = model.predict(
    X_test_std)  # verifico il modello sull'insieme di test definito prima, calcolo la cosiddetta previsione

print("Accuratezza:",
      accuracy_score(y_test, p_test))  # calcolo accuracy confrontando le previsioni con i dati corretti (y_test)

skplt.metrics.plot_confusion_matrix(p_test, y_test, figsize=(16, 10), x_tick_rotation=100)  # matrice di confusione
plt.show()

report = classification_report(p_test, y_test)  # vari indici statistici
print(report)