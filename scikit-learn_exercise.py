import io
# import certifi
import urllib.request

import matplotlib.pyplot as plt
import pandas as pd
import scikitplot as skplt
from sklearn.metrics import accuracy_score, classification_report
from sklearn.model_selection import train_test_split
from sklearn.neighbors import KNeighborsClassifier
from sklearn.preprocessing import StandardScaler

######################################## Fase 1 - Prelievo dataset
#print(certifi.where())

#dataset_20p_6f_576b_obf_TOY.parquet
#ds_file = io.BytesIO(urllib.request.urlopen('http://143.225.229.53:8080/share.cgi?ssid=cb287d7714fc428187835144faecc0b4&fid=cb287d7714fc428187835144faecc0b4&filename=dataset_20p_6f_576b_obf_TOY.parquet&openfolder=forcedownload&ep=').read())

#dataset_20p_6f_576b_no_obf_TOY.parquet
ds_file = io.BytesIO(urllib.request.urlopen('http://143.225.229.53:8080/share.cgi?ssid=61d98f9c74c04b5d86dbfa006fd23e26&fid=61d98f9c74c04b5d86dbfa006fd23e26&filename=dataset_20p_6f_576b_no_obf_TOY.parquet&openfolder=forcedownload&ep=').read())

#dataset_20p_6f_576b_obf_FULL.parquet
#ds_file = io.BytesIO(urllib.request.urlopen('http://143.225.229.53:8080/share.cgi?ssid=31a305e879644d7b943a5391a0ec0304&fid=31a305e879644d7b943a5391a0ec0304&filename=dataset_20p_6f_576b_obf_FULL.parquet&openfolder=forcedownload&ep=').read())

#dataset_20p_6f_576b_no_obf_FULL.parquet
#ds_file = io.BytesIO(urllib.request.urlopen('http://143.225.229.53:8080/share.cgi?ssid=1b0e364d6997445380b4f14219e6532e&fid=1b0e364d6997445380b4f14219e6532e&filename=dataset_20p_6f_576b_no_obf_FULL.parquet&openfolder=forcedownload&ep=').read())

#local
#df = pd.read_parquet('dataset_20p_6f_576b_obf_TOY.parquet')
#df = pd.read_parquet('dataset_20p_6f_576b_no_obf_TOY.parquet')
#df = pd.read_parquet('dataset_20p_6f_576b_obf_FULL.parquet')
#df = pd.read_parquet('dataset_20p_6f_576b_no_obf_FULL.parquet')

######################################## Fase 2 -  Filtraggio colonne dataframe

#remote
df = pd.read_parquet(ds_file)

y = df['LABEL'] #output

#print(df.columns)
#print(df)

columns_filtered = ['LABEL'] #Features: 'IAT', 'LOAD', 'PL', 'TTL', 'DIR', 'WIN', 'FLG'
#df.drop(columns_filtered, axis=1, inplace=True)
#print(df)


######################################## Fase 3 - Selezione dataset features e porzione di apprendimento

#transformers = [
#                ['scaler', RobustScaler(), [i for i in range(20)]],
#                #['one hot', OneHotEncoder(), [i for i in range(20)]]
#]
#
#ct = ColumnTransformer(transformers, remainder='passthrough')

columns_features = ['LOAD'] #Features: 'IAT', 'LOAD', 'PL', 'TTL', 'DIR', 'WIN', 'FLG'
#X = pd.DataFrame(df['IAT'].tolist())
#X = pd.DataFrame(df['LOAD'].tolist())
#X = pd.concat([X, X2], axis=1)
X=pd.DataFrame()
for i in range(0,len(columns_features),1):
  temp = pd.DataFrame(df[columns_features[i]].tolist())
  #print(temp)
  X=pd.concat([X,temp], axis=1)

print(X)

#column=1
#for column in X_plus:
  #print(df[column].values)
  #print(column)
#X= ct.fit_transform(X_plus)
#X=df

#print(X)

#print(y)
#print(df.columns)
#X=df

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.30) #Il dataset di test è composto dal 30% degli esempi complessivi

sc = StandardScaler() #La riduzione di scala standardizza la serie di valori. La standardizzazione è un metodo di
                      #riduzione di scala che trasforma una serie di valori in una distribuzione normale standard con media uguale a zero e devianza standard uguale a uno
sc.fit(X_train) #stima la media del campione e la deviazione standard dell'insieme di training (X_train).
X_train_std = sc.transform(X_train)
X_test_std = sc.transform(X_test)

#spline = SplineTransformer(degree=2, n_knots=3)
#spline.fit_transform(X_train_std)
#spline.fit_transform(X_test_std)


######################################## Fase 4 - Selezione modello e apprendimento algoritmo

model = KNeighborsClassifier()

#model = RandomForestClassifier()

#model = RadiusNeighborsClassifier(radius=200.0)

#model = MLPClassifier(hidden_layer_sizes=[200,200], max_iter=200, tol=0.000001)

#model = Perceptron(max_iter=200, tol=0.0000000001, eta0=0.00001, random_state=0)

#X_train = pd.DataFrame(X_train['LOAD'].tolist())
#X_test = pd.DataFrame(X_test['LOAD'].tolist())

model.fit(X_train_std, y_train) #comincio l'addestramento del modello


######################################## Fase 5 - Verifica modello e analisi prestazoni

#p_train = model.predict(X_train)

p_test = model.predict(X_test_std) #verifico il modello sull'insieme di test definito prima, calcolo la cosiddetta previsione

#mae_train = mean_absolute_error(y_train, p_test)
#mae_test = mean_absolute_error(y_test, p_test)

#print(accuracy_score(y_train, p_train))

print("Accuratezza:", accuracy_score(y_test, p_test)) #calcolo accuracy confrontando le previsioni con i dati corretti (y_test)

#plot_decision_regions(X_test, y_test, clf=model, legend=2) # Regioni di decisione (applicabile solo fino a 2 features)
#plt.show()

#for i in range(0,len(X_test_std)):
#  z = np.array([X_test_std[i]])
#  y = model.predict(z)
#  print(X_test_std, y_test[i], y)

skplt.metrics.plot_confusion_matrix(p_test,y_test, figsize=(16, 10), x_tick_rotation=100) #matrice di confusione
plt.show()

report=classification_report(p_test, y_test) #vari indici statistici
print(report)